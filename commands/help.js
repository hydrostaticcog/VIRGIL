const Discord = require("discord.js")
Logger = require('../modules/logger')
logger = new Logger('main');

module.exports = {
    name: 'help',
    description: "this command helps people!",
    execute(message, args){
        const helpEmbed = new Discord.MessageEmbed()
            .setColor('#1eacb0')
            .setTitle('So you need help, eh?')
            .addFields(
                { name: 'Help: ', value: 'This is the only command, for now...'}
            )
            .setTimestamp()
        message.author.send(helpEmbed)
    }
}